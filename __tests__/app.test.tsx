import * as React from 'react';
import App from '../src/app';
import * as renderer from 'react-test-renderer';

describe('App (Snapshot)', () => {
  it('App renders app is working', () => {
    const component = renderer.create(<App />);
    const json = component.toJSON();
    expect(json).toMatchSnapshot();
  });
});